import { connect } from "react-redux"
import { useEffect } from "react";
import LoadingCard from "components/loaders/LoadingCard";
import SmallSetPagination from "components/paginacion/SmallSetPagination";
import { get_store_list, get_store_list_page } from "redux/actions/storehouse";
import StoreCard from "./StoreCard";


function StoreListComponent({
    get_store_list,
    get_store_list_page,
    store_list,
    count
}) {
    useEffect(() => {
        get_store_list()
    }, [])

    return (
        <div>
            {
                store_list ?
                    <>
                        <div className="relative bg-gray-50 pb-8 px-4 sm:px-6 lg:pb-12 lg:px-8">
                            <div className="absolute inset-0">
                                <div className="bg-white h-1/3 sm:h-2/3" />
                            </div>
                            <div className="relative max-w-7xl mx-auto">

                                <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
                                    {
                                        store_list.map(store => (
                                            <StoreCard data={store} />
                                        ))
                                    }
                                </div>
                                <SmallSetPagination
                                    get_product_list_page={get_store_list_page}
                                    product_list={store_list}
                                    count={count}
                                />
                            </div>
                        </div>
                    </>
                    :
                    <LoadingCard />
            }
        </div>
    )

}

const mapStateToProps = state => ({
    store_list: state.storehouses.storehouse_list,
    count: state.count
})

export default connect(mapStateToProps, {
    get_store_list,
    get_store_list_page
})(StoreListComponent)