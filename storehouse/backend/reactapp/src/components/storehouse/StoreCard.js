import { Link } from "react-router-dom"

function StoreCard(data) {
    let store = data && data.data

    return (
        <div>
            <div className="flex flex-col rounded-lg shadow-lg overflow-hidden">
                <Link to={`/store/${store.store_slug}`} className="block mt-2">
                <div className="flex-shrink-0">
                    <img className="h-48 w-full object-cover" src={process.env.REACT_APP_API_URL + store.image} />
                </div>
                    <div className="flex-1 bg-white p-6 flex flex-col justify-between">
                        <div className="flex-1">
                            <p className="text-sm font-medium text-indigo-600"></p>
                            <p className="text-xl font-semibold text-gray-900">{store.name}</p>
                        </div>
                    </div>
                </Link>
            </div>
        </div>
        
    )
}

export default StoreCard