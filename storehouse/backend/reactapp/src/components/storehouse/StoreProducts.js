import { connect } from "react-redux"
import { useEffect } from "react";
import LoadingCard from "components/loaders/LoadingCard";
import SmallSetPagination from "components/paginacion/SmallSetPagination";
// import { get_product_list_page } from "redux/actions/product";
import { get_store_products, get_store_products_page } from "redux/actions/storehouse";
import Header from "components/navigation/Header";

import { useParams } from "react-router-dom";
import StoreProductCard from "./StoreProductCard";


function StoreProducts({
    get_store_products,
    store_products,
    count,
    st
}) {
    const params = useParams()
    const slug = params.slug
    

    useEffect(() => {
        get_store_products(slug)
    }, [])
    return (
        <div>
            {
                store_products ?
                    <>
                        <Header data={store_products[0].store.name} />

                        <div className="relative bg-gray-50 pb-8 px-4 sm:px-6 lg:pb-12 lg:px-8">
                            <div className="absolute inset-0">
                                <div className="bg-white h-1/3 sm:h-2/3" />
                            </div>
                            <div className="relative max-w-7xl mx-auto">

                                <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
                                    {
                                        store_products.map(store_product => (
                                            <StoreProductCard data={store_product} />
                                        ))
                                    }
                                </div>
                                <SmallSetPagination
                                    get_product_list_page={get_store_products_page}
                                    product_list={store_products}
                                    count={count}
                                    slug={slug}
                                />
                            </div>
                        </div>

                    </>
                    :
                    <LoadingCard />
            }
        </div>
    )
}

const mapStateToProps = state => ({
    store_products: state.storehouses.storehouse_products,
    count: state.storehouses.count,
    st: state.storehouses
})

export default connect(mapStateToProps, {
    get_store_products,
})(StoreProducts)