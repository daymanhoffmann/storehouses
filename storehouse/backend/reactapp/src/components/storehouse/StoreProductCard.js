import { Link } from "react-router-dom"

function StoreProductCard(data) {
    let info = data && data.data
    return (
        <div>
            <Link to={`/product/${info.product.product_slug}`} className="block mt-2">
                <div className="flex-1">
                        <p className="text-xl font-semibold text-indigo-600">{info.store.name}</p>
                    </div>
                <div className="flex flex-col rounded-lg shadow-lg overflow-hidden">
                    <div className="flex-shrink-0">
                        <img className="h-48 w-full object-cover" src={process.env.REACT_APP_API_URL + info.product.image} />
                    </div>
                    <div className="flex-1 bg-white p-6 flex flex-col justify-between">
                        <div className="flex-1">

                            <p className="text-xl font-semibold text-gray-900">{info.product.name}</p>
                            <p className="mt-3 text-base text-gray-500">{info.product.description}</p>
                            <p className="text-xl font-semibold text-gray-900">{info.category.name}</p>
                            <p className="text-xl font-semibold text-gray-900 text-right">{info.quantity} ud</p>
                            <p className="text-xl font-semibold text-gray-900 text-right">{info.product.price} €</p>

                        </div>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default StoreProductCard