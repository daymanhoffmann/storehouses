import LoadingCard from "components/loaders/LoadingCard"
import SmallSetPagination from "components/paginacion/SmallSetPagination"
import { useEffect } from "react"
import { connect } from "react-redux"
import StoreCard from "components/storehouse/StoreCard"
import StoreProductCard from "components/storehouse/StoreProductCard"

function ProductList({
    products_list,
    get_product_list_page,
    count
}) {
    return (
        <div>
            {
                products_list ?
                    <>
                        <div className="relative bg-gray-50 pb-8 px-4 sm:px-6 lg:pb-12 lg:px-8">
                            <div className="absolute inset-0">
                                <div className="bg-white h-1/3 sm:h-2/3" />
                            </div>
                            <div className="relative max-w-7xl mx-auto">

                                <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
                                    {
                                        products_list.map(prod => (
                                            <StoreProductCard data={prod}/>
                                        ))
                                    }
                                </div>
                                <SmallSetPagination
                                    get_product_list_page={get_product_list_page}
                                    products_list={products_list}
                                    count={count}
                                />
                            </div>
                        </div>
                    </>
                    :
                    <LoadingCard />
            }
        </div>
    )
}

const mapStateToProps = state => ({
    count: state.count
})

export default connect(mapStateToProps, {
})(ProductList)