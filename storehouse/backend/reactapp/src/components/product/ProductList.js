import { connect } from "react-redux"
import { useEffect } from "react";
import LoadingCard from "components/loaders/LoadingCard";
import ProductCard from "./ProductCard";
import SmallSetPagination from "components/paginacion/SmallSetPagination";
import { get_product_list, get_product_list_page } from "redux/actions/product";

function ProductList({
    get_product_list,
    get_product_list_page,
    product_list,
    count
}) {
    useEffect(() => {
        get_product_list()
    }, [])

    return (
        <div>
            {
                product_list ?
                    <>

                        <div className="relative bg-gray-50 pb-8 px-4 sm:px-6 lg:pb-12 lg:px-8">
                            <div className="absolute inset-0">
                                <div className="bg-white h-1/3 sm:h-2/3" />
                            </div>
                            <div className="relative max-w-7xl mx-auto">

                                <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
                                    {
                                        product_list.map(product => (
                                            <ProductCard data={product} />
                                        ))
                                    }
                                </div>
                                <SmallSetPagination
                                    get_product_list_page={get_product_list_page}
                                    product_list={product_list}
                                    count={count}
                                />
                            </div>
                        </div>

                    </>
                    :
                    <LoadingCard />
            }
        </div>
    )
}

const mapStateToProps = state => ({
    product_list: state.product.product_list,
    count: state.product.count
})

export default connect(mapStateToProps, {
    get_product_list,
    get_product_list_page
})(ProductList)