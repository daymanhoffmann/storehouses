import FullWidthLayout from "hocs/layouts/FullWidthLayout";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { search_products } from "redux/actions/product";
import ProductListSearch from "components/product/ProductListSearch";
import { connect } from "react-redux";
 
function Search({
    search_products,
    products
}) {
    const params = useParams()
    const term = params.term

    useEffect(() => {
        search_products(term)
    }, [])

    return (
        <FullWidthLayout>
            <ProductListSearch products_list={products} />
        </FullWidthLayout>
    )
}

const mapStateToProps = state => ({
    products: state.product.product_list
})

export default connect(mapStateToProps, {
    search_products
})(Search)