import Header from "components/navigation/Header";
import ProductsList from "components/product/ProductList";
import FullWidthLayout from "hocs/layouts/FullWidthLayout";
import { connect } from "react-redux"


function ProductList({

}) {

    return (
        <FullWidthLayout>
            <Header data={'Products'}/>
            <ProductsList />
        </FullWidthLayout>
    )
}

const mapStateToProps = state => ({
})

export default connect(mapStateToProps, {

})(ProductList)