import Header from "components/navigation/Header";
import ProductsList from "components/product/ProductList";
import StoreListComp from "components/storehouse/StoreListComp";
import FullWidthLayout from "hocs/layouts/FullWidthLayout";
import { connect } from "react-redux"

function StoreList({

}) {

    return (
        <FullWidthLayout>
            <Header data={'Stores'}/>
            <StoreListComp />
        </FullWidthLayout>
    )
}

const mapStateToProps = state => ({
})

export default connect(mapStateToProps, {

})(StoreList)