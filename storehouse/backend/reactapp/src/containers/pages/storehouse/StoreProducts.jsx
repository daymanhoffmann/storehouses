import FullWidthLayout from "hocs/layouts/FullWidthLayout";
import { connect } from "react-redux"
import StoreProducts from "components/storehouse/StoreProducts";

function StoreProductsContainer({

}) {

    return (
        <FullWidthLayout>
            <StoreProducts />
        </FullWidthLayout>
    )
}

const mapStateToProps = state => ({
})

export default connect(mapStateToProps, {

})(StoreProductsContainer)