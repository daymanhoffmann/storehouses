import {
    GET_PRODUCT_LIST_SUCCESS,
    GET_PRODUCT_LIST_FAIL,
    GET_PRODUCT_SUCCESS,
    GET_PRODUCT_FAIL,
    GET_SEARCH_PRODUCTS_SUCCESS,
    GET_SEARCH_PRODUCTS_FAIL
} from "../actions/types"

const initialState = {
    product_list: null,
    product_list_category: null,
    count: null,
    next: null,
    previous: null,
    product: null
};


export default function products(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case GET_PRODUCT_LIST_SUCCESS:
            return {
                ...state,
                product_list: payload.results.products,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_PRODUCT_LIST_FAIL:
            return {
                ...state,
                store_list_category: null,
                count: null,
                next: null,
                previous: null,
            }
        case GET_PRODUCT_SUCCESS:
            return {
                ...state,
                product: payload.product,
            }
        case GET_PRODUCT_FAIL:
            return {
                ...state,
                product: null,
            }
        case GET_SEARCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                product_list: payload.results.filtered_products,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_SEARCH_PRODUCTS_FAIL:
            return {
                ...state,
                store_list_category: null,
                count: null,
                next: null,
                previous: null,
            }
        default:
            return state
    }


}