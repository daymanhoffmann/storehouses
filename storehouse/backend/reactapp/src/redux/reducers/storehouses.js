import {
    GET_STORES_LIST_SUCCESS,
    GET_STORES_LIST_FAIL,
    GET_STORE_PRODUCTS_SUCCESS,
    GET_STORE_PRODUCTS_FAIL
} from '../actions/types';

const initialState = {
    storehouse_list: null,
    count: null,
    next: null,
    previous: null, 
    storehouse_products: null
};

export default function storehouses(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case GET_STORES_LIST_SUCCESS:
            return {
                ...state,
                storehouse_list: payload.results.stores,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_STORES_LIST_FAIL:
            return {
                ...state,
                storehouse_list: null,
                count: null,
                next: null,
                previous: null,
            }
        case GET_STORE_PRODUCTS_SUCCESS:
            return {
                ...state,
                storehouse_products: payload.results.stock,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_STORE_PRODUCTS_FAIL:
            return {
                ...state,
                storehouse_products: null,
                count: null,
                next: null,
                previous: null,
            }
        default:
            return state
    }
};