import { combineReducers } from "redux";
import product from './product';
import categories from "./categories";
import storehouses from "./storehouses";
// import search from "./stock";


export default combineReducers({
    product,
    categories,
    storehouses,
    // search,
})