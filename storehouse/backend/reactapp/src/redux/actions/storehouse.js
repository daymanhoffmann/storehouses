import axios from "axios";

import {
    GET_STORES_LIST_SUCCESS,
    GET_STORES_LIST_FAIL,
    GET_STORE_PRODUCTS_SUCCESS,
    GET_STORE_PRODUCTS_FAIL
} from "./types"

export const get_store_list = () => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/stores/`, config);
        if (res.status === 200) {
            dispatch({
                type: GET_STORES_LIST_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_STORES_LIST_FAIL
            });
        }
    } catch {
        dispatch({
            type: GET_STORES_LIST_FAIL
        });
    }
}

export const get_store_list_page = (slug, p) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {

        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/stores/?p=${p}`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_STORES_LIST_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_STORES_LIST_FAIL
            });
        }

    } catch {
        dispatch({
            type: GET_STORES_LIST_FAIL
        });
    }
}

export const get_store_products = (slug) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/store/${slug}/`, config);
        if (res.status === 200) {
            dispatch({
                type: GET_STORE_PRODUCTS_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_STORE_PRODUCTS_FAIL
            });
        }
    } catch (err) {
        dispatch({
            type: GET_STORE_PRODUCTS_FAIL
        });
    }
};


export const get_store_products_page = (slug, p) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {

        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/store/${slug}/?p=${p}`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_STORES_LIST_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_STORES_LIST_FAIL
            });
        }

    } catch {
        dispatch({
            type: GET_STORES_LIST_FAIL
        });
    }
}
