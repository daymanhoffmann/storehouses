import axios from "axios";
// import { useDispatch } from "react-redux";

import {
    GET_PRODUCT_LIST_SUCCESS,
    GET_PRODUCT_LIST_FAIL,
    GET_PRODUCT_SUCCESS,
    GET_PRODUCT_FAIL,
    GET_SEARCH_PRODUCTS_SUCCESS,
    GET_SEARCH_PRODUCTS_FAIL
} from "./types"

export const get_product_list = () => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/products/`, config);
        if (res.status === 200) {
            dispatch({
                type: GET_PRODUCT_LIST_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_PRODUCT_LIST_FAIL
            });
        }
    } catch {
        dispatch({
            type: GET_PRODUCT_LIST_FAIL
        });
    }
}

export const get_product_list_page = (slug, p) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/products/?p=${p}`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_PRODUCT_LIST_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_PRODUCT_LIST_FAIL
            });
        }

    } catch {
        dispatch({
            type: GET_PRODUCT_LIST_FAIL
        });
    }
}

export const get_product = (slug) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/product/${slug}/`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_PRODUCT_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_PRODUCT_FAIL
            });
        }
    } catch (err) {
        dispatch({
            type: GET_PRODUCT_FAIL
        });
    }
};

export const search_products = (search_term) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/search/${search_term}/`, config);
        if (res.status === 200) {
            dispatch({
                type: GET_SEARCH_PRODUCTS_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_SEARCH_PRODUCTS_FAIL
            });
        }
    } catch (err) {
        dispatch({
            type: GET_SEARCH_PRODUCTS_FAIL
        });
    }
};