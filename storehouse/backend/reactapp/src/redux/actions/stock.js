// import axios from "axios";

// import {
//     GET_SEARCH_PRODUCTS_SUCCESS,
//     GET_SEARCH_PRODUCTS_FAIL
// } from "./types"




// export const search_products = (search_term) => async dispatch => {

//     const config = {
//         headers: {
//             'Accept': 'application/json'
//         }
//     };

//     try {
//         const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/search/${search_term}/`, config);
//         if (res.status === 200) {
//             dispatch({
//                 type: GET_SEARCH_PRODUCTS_SUCCESS,
//                 payload: res
//             });
//         } else {
//             dispatch({
//                 type: GET_SEARCH_PRODUCTS_FAIL
//             });
//         }
//     } catch (err) {
//         dispatch({
//             type: GET_SEARCH_PRODUCTS_FAIL
//         });
//     }
// };