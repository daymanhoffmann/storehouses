import Error404 from "containers/errors/Error404";
// import Home from "containers/pages/Home";
import store from "store";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import ProductList from "containers/pages/product/ProductList";
import Product from "containers/pages/product/Product";
import StoreList from "containers/pages/storehouse/StoreList";
import StoreProductsContainer from "containers/pages/storehouse/StoreProducts";
import Search from "containers/pages/search";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          {/*Error Display */}
          <Route path="*" element={<Error404 />} />

          {/* Home Display */}         
          <Route path="/" element={<StoreList />} />

          <Route path="/store/:slug" element={<StoreProductsContainer />} />          
          <Route path="/search/:term" element={<Search/>}/>
          <Route path="/products" element={<ProductList />} />
          <Route path="/product/:slug" element={<Product />} />
          <Route path="/admin" element={<Product />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
