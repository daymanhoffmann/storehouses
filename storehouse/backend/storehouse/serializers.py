from rest_framework import serializers

from .models import Product, Category, Storehouse, Stock


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storehouse
        fields = [
            'name',
            'image',
            'store_slug'
        ]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'name',
        ]


class ProductSerializer(serializers.ModelSerializer):
    image = serializers.CharField(source='get_img')

    class Meta:
        model = Product
        fields = [
            'name',
            'description',
            'price',
            'product_slug',
            'image',
        ]


class StockSerializer(serializers.ModelSerializer):
    store = StoreSerializer(read_only=True)
    category = CategorySerializer(read_only=True)
    product = ProductSerializer(read_only=True)

    class Meta:
        model = Stock
        fields = [
            'quantity',
            'product',
            'store',
            'category',
        ]

class ProductQuantitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Product