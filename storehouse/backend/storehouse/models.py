from django.db import models
from django.utils.html import escape


def product_path(instance, filename):
    name = '_'.join(instance.name.split(' '))
    return f'store/{name}/{filename}'


class Storehouse(models.Model):
    name = models.CharField(max_length=200, null=False)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')
    store_slug = models.SlugField(unique=True, db_index=True)
    image = models.ImageField(upload_to=product_path, null=True)

    class Meta:
        verbose_name = 'Storehouse'
        verbose_name_plural = 'Storehouses'
        db_table = 'store'

    def __str__(self):
        return self.name


class Category(models.Model):

    name = models.CharField(max_length=200, null=False)
    # store = models.ManyToManyField(Storehouse, related_name="categories")
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        db_table = 'category'

    def __str__(self):
        return self.name


def product_path(instance, filename):
    return f'product/{instance.name}/{filename}'


class Product(models.Model):
    name = models.CharField(max_length=200, null=False, unique=True)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Creation Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='update date')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    product_slug = models.SlugField(unique=True, db_index=True)
    image = models.ImageField(upload_to=product_path)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        db_table = 'product'
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_img(self):
        if self.image:
            return self.image.url
        return ''


class Stock(models.Model):
    quantity = models.IntegerField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    store = models.ForeignKey(Storehouse, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Stock'
        verbose_name_plural = 'Stocks'
        db_table = 'stock'
        unique_together = ("product", "store", "category"),
        index_together = [("product", "store", "category"),]

    def __str__(self):
        return f'{self.product.name}'
