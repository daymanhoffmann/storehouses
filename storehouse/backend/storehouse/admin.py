from django.contrib import admin
from .models import Storehouse, Product, Category, Stock



class StockInline(admin.StackedInline):
    model = Stock
    extra = 0


@admin.register(Storehouse)
class StorehouseAdmin(admin.ModelAdmin):
    inlines = [StockInline]
    search_fields = ('name',)

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [StockInline]
    search_fields = ('name',)


@admin.register(Category)
class CategotyAdmin(admin.ModelAdmin):
    inlines = [StockInline]
    search_fields = ('name',)


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ('id', 'store', 'category', 'product', 'quantity',)
    ordering = ('id', 'store', 'category', 'product', 'quantity',)
    list_editable = ('quantity',)
    list_filter = ('product','category',)
