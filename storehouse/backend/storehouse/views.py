from django.shortcuts import render, get_object_or_404
from django.db.models import F

from rest_framework.response import Response
from rest_framework import status
# from rest_framework import permissions
from rest_framework.views import APIView

from .pagination import SmallSetPagination, MediumSetPagination
from .models import Category, Product, Storehouse, Stock
from .serializers import (StoreSerializer, ProductSerializer,
                          CategorySerializer, StockSerializer)

from django.db.models.query_utils import Q


class StoreHouseListView(APIView):
    # to implement security
    # permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        if Storehouse.objects.all().exists():
            strores = Storehouse.objects.all()
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(strores, request)
            serializer = StoreSerializer(results, many=True)

            return paginator.get_paginated_response({'stores': serializer.data})
        else:
            return Response({'error': 'No StoreHouse found'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StockByStoreView(APIView):

    def get(self, request, store_slug, format=None):
        if Stock.objects.all().exists():
            stock = Stock.objects.filter(store__store_slug=store_slug)
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(stock, request)
            serializer = StockSerializer(results, many=True)

            return paginator.get_paginated_response({'stock': serializer.data})
        else:
            return Response({'error': 'No STOCK found'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StockListView(APIView):

    def get(self, request, format=None):
        if Stock.objects.all().exists():
            stock = Stock.objects.all()
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(stock, request)
            serializer = StockSerializer(results, many=True)

            return paginator.get_paginated_response({'stock': serializer.data})
        else:
            return Response({'error': 'No STOCK found'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProductListView(APIView):

    def get(self, request, format=None):
        if Product.objects.all().exists():
            products = Product.objects.all()
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(products, request)
            serializer = ProductSerializer(results, many=True)

            return paginator.get_paginated_response({'products': serializer.data})
        else:
            return Response({'error': 'No Products found'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProductDetailView(APIView):
    def get(self, request, product_slug, format=None):
        product = get_object_or_404(Product, product_slug=product_slug)
        serializer = ProductSerializer(product)
        return Response({'product': serializer.data},
                        status=status.HTTP_200_OK)

    def put(self, request, product_slug, *args, **kwargs):
        product = get_object_or_404(Product, product_slug=product_slug)
        serializer = ProductSerializer(product, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, product_slug, format=None):
        product = Stock.objects.filter(product__product_slug=product_slug)
        if product.exists:
            action = request.data
            if action.get('increment'):
                increment = action['increment']
                product.update(quantity=F('quantity') + increment)
                updated_product = get_object_or_404(
                    Product, product_slug=product_slug)
                serializer = ProductSerializer(updated_product)
                return Response({'product': serializer.data},
                                status=status.HTTP_200_OK)
        return Response({'error': f'No Product found with slug {product_slug}'},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class StoreHouseProducts(APIView):
    def get(self, request, store_slug, format=None):
        if Category.objects.all().exists() and Category.objects.filter(store__store_slug=store_slug):
            categories = Category.objects.filter(store__store_slug=store_slug)
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(categories, request)
            serializer = CategorySerializer(results, many=True)

            return paginator.get_paginated_response({'categories': serializer.data})
        else:
            return Response({'error': 'No Categories found'},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProductSearchViews(APIView):

    def get(self, request, search_term):
        matches = Stock.objects.filter(
            Q(product__name__icontains=search_term) |
            Q(store__name__icontains=search_term) |
            Q(category__name__icontains=search_term)
        )

        paginator = SmallSetPagination()
        result = paginator.paginate_queryset(matches, request)
        seriaizer = StockSerializer(result, many=True)
        return paginator.get_paginated_response({'filtered_products': seriaizer.data})
        # return Response({'filtered_products': seriaizer.data}, status=status.HTTP_200_OK)