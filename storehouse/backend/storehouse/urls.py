from django.conf.urls.static import static
from django.urls import path
from .views import (StoreHouseListView, ProductListView,
                    ProductDetailView, StockByStoreView, StockListView,
                    ProductSearchViews
                    )
from django.conf import settings

urlpatterns = [
    path('stores/', StoreHouseListView.as_view()),
    path('products/', ProductListView.as_view()),
    path('product/<product_slug>/', ProductDetailView.as_view()),
    # path('<store_slug>/categories', CategoriesListView.as_view()),
    path('stock/',  StockListView.as_view()),
    path('store/<store_slug>/', StockByStoreView.as_view()),
    path('search/<search_term>/', ProductSearchViews.as_view()),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
